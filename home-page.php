<?php
/**
 * Template Name: Home Page
 *
 */

 get_header(); ?>
<?php if (has_post_thumbnail( $post->ID ) ): ?>
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
	<div class="jumbotron" style="background-image: url('<?php echo $image[0]; ?>')">
		<div class="container">
			<div class="search-home">
				<?php dynamic_sidebar( 'homepage_hero' ); ?>
			</div>
		</div>
	</div>
<?php else: ?>
	<section class="jumbotron">
		<div class="container">
			<h1><?php the_title(); ?></h1>
			<div class="search">
				<?php get_search_form(); ?>
			</div>
		</div>
	</section>
<?php endif; ?>
	<section class="home announcements">
		<div class="container">
			<?php dynamic_sidebar( 'announcements' ); ?>
		</div>
	</section>
	<section class="home categories">
		<div class="container">
			<?php dynamic_sidebar( 'categories' ); ?>
		</div>
	</section>
	<section class="home recent-files">
		<div class="container">
			<?php dynamic_sidebar( 'recent' ); ?>
		</div>
	</section>


	<?php if (have_posts()) while (have_posts()) : the_post(); ?>
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<?php //the_title('<h1>', '</h1>'); ?>
					<?php the_content(); ?>
					
				</div>
			</div>
		</div>
	<?php endwhile; ?>
<?php get_footer(); ?>