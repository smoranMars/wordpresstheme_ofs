<?php
/**
 * The sidebar containing the main widget area
 *
 */

if ( is_active_sidebar( 'search_sidebar' )  ) : ?>
	<div id="secondary" class="secondary">

		<?php if ( is_active_sidebar( 'search_sidebar' ) ) : ?>
			<div id="widget-area" class="widget-area" role="complementary">
				<?php dynamic_sidebar( 'search_sidebar' ); ?>
			</div><!-- .widget-area -->
		<?php endif; ?>

	</div><!-- .secondary -->

<?php endif; ?>
