<?php 
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 */

get_header(); ?>
	
		<div class="container">
			<div class="row">
				<div class="col-xs-12 single">
				<?php if (have_posts()): ?>
					<?php
						// Start the loop.
						while ( have_posts() ) : the_post(); ?>
							<h1><?php the_title(); ?></h1>
							<?php the_content('Read More...');
							

						/*
						* Include the post format-specific template for the content. If you want to
						* use this in a child theme, then include a file called called content-___.php
						* (where ___ is the post format) and that will be used instead.
						*/
						

						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
						comments_template();
						endif;

						// Previous/next post navigation.
						the_post_navigation( array(
							'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next', 'ofs' ) . '</span> ' .
							'<span class="screen-reader-text">' . __( 'Next post:', 'ofs' ) . '</span> ' .
							'<span class="post-title">%title</span>',
							'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Previous', 'ofs' ) . '</span> ' .
							'<span class="screen-reader-text">' . __( 'Previous post:', 'ofs' ) . '</span> ' .
							'<span class="post-title">%title</span>',
						) );

						// End the loop.
						endwhile;
					?>
				<?php endif; ?>
				</div>
			</div>
		</div>

<?php get_footer(); ?>