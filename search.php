<?php get_header(); ?>
	<section class="search jumbotron" style="background-image: url('/wp-content/uploads/2015/08/insideFireTruckHeader.jpg')">
		<div class="container">
			<h1 class="search-headline"><?php echo (isset($_GET['_sft_category']) ? $_GET['_sft_category'] : "All Categories"); ?></h1>
		</div>
	</section>

	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-4">
				<?php get_sidebar(); ?>
			</div>
			<div class="col-xs-12 col-sm-8 search-filter-results">
				<?php 
					if ( have_posts() ) {
						while ( have_posts() ) {
							the_post(); 
							$fileLink = types_render_field('file-upload', array("raw"=>"true","separator"=>";")); 
							$video = types_render_field('youtube', array("raw"=>"false","separator"=>";")); 
							$image = types_render_field('image', array("raw"=>"true","separator"=>";")); 
							$webpage = types_render_field('web-page', array("raw"=>"true","separator"=>";")); 
							$postType = get_post_type();
							?>
							<div class="result <?php echo $postType;?>">
								<?php if ($postType == 'files'): ?>
									<h3><a href="<?php echo $fileLink; ?>" target="_blank"><?php the_title(); ?></a></h3>

								<?php elseif($postType == 'webpages'):?>
									<h3><a href="<?php echo $webpage; ?>" target="_blank"><?php the_title(); ?></a></h3>

								<?php elseif($postType == 'video' || $postType == 'image'):	?>
									<h3><a data-toggle="modal" data-target="#modal-<?php the_ID(); ?>"><?php the_title(); ?></a></h3>
									<!-- Modal -->
										<div class="modal fade" id="modal-<?php the_ID(); ?>" tabindex="-1" role="dialog" aria-labelledby="modal-label-<?php the_ID(); ?>">
										  <div class="modal-dialog" role="document">
										    <div class="modal-content">
										      <div class="modal-header">
										        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										      </div>
										      <div class="modal-body">
										      	<?php if ($postType == 'video'): 
										      		echo $video;
										      	else : ?>
										        	 <a href="<?php echo $fileLink; ?>"><img src="<?php echo $fileLink; ?>" alt="<?php the_title(); ?>" /></a>
										       	<?php endif; ?>
										      </div>
										    </div>
										  </div>
										</div>
								
								
								<?php endif; ?>
									<div class="date-updated">
										<?php the_modified_date('F j, Y', 'Last Updated: ');?>
									</div>
									<?php 
										$description = get_the_content();
										
										if ($description): ?>
											<a class="details-collapse" data-toggle="collapse" href="#details-<?php the_ID(); ?>" aria-expanded="false" aria-controls="collapseExample">Details <i class="fa fa-caret-right"></i></a>
											<div class="description collapse" id="details-<?php the_ID(); ?>">
												
												<?php print $description; ?>
											</div>
									<?php endif; ?>

									<div class="categories-tree">
										<?php echo do_shortcode('[wpv-category-tree]'); ?>
									</div>
								</div>
							<?php


						} // end while
					} else {
						echo "Your search did not return any results, Please try again";
					} 
					?>
					<?php if ( $wp_query->max_num_pages > 1 ) : ?>
					<div class="pages">
					<?php
						global $wp_query;

						$big = 999999999; // need an unlikely integer

						echo paginate_links( array(
							'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
							'format' => '?paged=%#%',
							'current' => max( 1, get_query_var('paged') ),
							'total' => $wp_query->max_num_pages
						) );
					?>
					</div>
					<?php endif; ?>
				
			</div>
			<?php
				
			?>
		</div>
	</div>


<?php get_footer(); ?>