(function ($) {
    function filterClasses () {

        $('.active').removeClass('active');
        $(this).parent().parent().addClass('active');
        $('input:not(:checked)').parentsUntil('.sf-field-category', 'li').removeClass("active-trail");
        $('input:checked').parentsUntil('.sf-field-category', 'li').addClass("active-trail");

        setTitle();
    }

    function setTitle () {
        var title = $('.sf-field-category ul > li.active-trail > label input').val();
        //console.log('current title' + title);

        if(title == 0) {
            title = 'All Categories';
        }
        
        if(title) {
            $('h1.search-headline').html(title);
        }
    }
    function targetBlank () {
        $.each($(".file-open-blank"), function () {
            $(this).attr('target', '_blank');
        });
    }

    function disclaimerModal () {
        if(document.cookie.replace(/(?:(?:^|.*;\s*)disclaimer_agreed\s*\=\s*([^;]*).*$)|^.*$/, "$1") == false) {
            $('#disclaimer-modal').modal('show');

            $('.disclaimer-agreement').bind('click', function() {
                document.cookie = "disclaimer_agreed=true; expires=Fri, 31 Dec 9999 23:59:59 UTC";
            });
        }
    }

    $(document).ready(function() {
        targetBlank();
        disclaimerModal();

        $('input:checked').addClass('active').parentsUntil('.sf-field-category', 'li').addClass("active-trail").bind(function(){
            setTitle();
        });

        $('input').click(function () {
            filterClasses();
        });    
    });
})(jQuery);