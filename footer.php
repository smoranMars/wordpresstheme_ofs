			<footer class="footer">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-4">
							<?php dynamic_sidebar( 'footer_left' ); ?>
						</div>
						<div class="col-xs-12 col-sm-4">
							<?php dynamic_sidebar( 'footer_center' ); ?>
						</div>
						<div class="col-xs-12 col-sm-4">
							<?php dynamic_sidebar( 'footer_right' ); ?>
						</div>
					</div>
				</div>
				<?php ofs_theme_disclaimer_modal('The materials that are available through this online depository are intended only for internal use by Ottawa Fire Services staff and Ottawa Fire Services volunteers. The materials including any information found in this repository is provided “as is” and any use by person(s) other than current Ottawa Fire Services staff and/or Ottawa Fire Services Volunteers is at their own risk. The City will not be liable for any loss or damage arising from third party use of the materials and/or use that falls outside the scope of Ottawa Fire Services operations.', 'Disclaimer'); ?>
			</footer>
		</div><!--	#wrapper	-->
		<?php wp_footer(); ?>
	</body>
</html> 