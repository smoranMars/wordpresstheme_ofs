<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

<head>	
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo get_the_title(); ?> | <?php bloginfo('name'); ?></title>
	<?php wp_head(); ?>
</head>
<body>
	<div id="wrapper" class="<?php if(is_front_page()) echo " home"; ?>">
		<header class="header navbar navbar-default navbar-inverse" style="<?php echo 'background-color:'.esc_url( get_theme_mod( 'ofs_nav_color' ) ); ?>">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-5">
						<?php if ( get_theme_mod( 'ofs_logo' ) ) : ?>
							<div class='site-logo'>
								<a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'>
									<img src='<?php echo esc_url( get_theme_mod( 'ofs_logo' ) ); ?>' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'>
								</a>
							</div>
						<?php else : ?>
							<a class="navbar-brand" href="<?php bloginfo('url'); ?>"><?php bloginfo('name');?></a>
						<?php endif; ?>
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					        <span class="sr-only">Toggle navigation</span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					    </button>
				    </div>
					<div class="collapse navbar-collapse nav-block col-xs-12 col-sm-6 col-md-7 pull-right" id="bs-example-navbar-collapse-1">
						<?php wp_nav_menu( 
							array( 
								'theme_location' => 'primary',
								'menu_class' => 'nav navbar-nav'
							) ); 
						?>


					</div><!-- /.navbar-collapse -->
					
				</div>
			</div>
			
		</header><!--	.header	-->


		