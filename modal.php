<div id="disclaimer-modal" class="modal fade" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><?php echo $title?></h4>
      </div>
      <div class="modal-body">
        <p><?php echo $body?></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default disclaimer-agreement" data-dismiss="modal">OK</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->