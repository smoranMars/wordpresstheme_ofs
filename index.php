<?php get_header(); ?>

	<div class="container">
		<div class="row">
			<div class="col-xs-4">
				<?php //dynamic_sidebar('search_sidebar'); ?>
			</div>
			<div class="col-xs-8 search-filter-results">
				<?php 
					if ( have_posts() ) {
						while ( have_posts() ) {
							the_post(); ?>
							<div class="result <?php echo get_post_type();?>">
								<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
								<?php the_excerpt(); ?>
								<div class="date-updated">
									<?php the_modified_date('F j, Y', 'Last Updated: ');?>
								</div>
							</div>
							<?php
						} // end while
					} // end if
				?>
			</div>
		</div>
	</div>


<?php get_footer(); ?>