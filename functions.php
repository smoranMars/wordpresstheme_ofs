<?php 

function ofs_settings() {
	add_theme_support( 'post-thumbnails' ); 
}

add_action( 'after_setup_theme', 'ofs_settings' );

function ofs_scripts() {
	//fonts
	wp_register_style('openSans', '//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800');
	wp_register_style('fontAwesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');
	
	//styles
	wp_enqueue_style( 'openSans');
	wp_enqueue_style( 'fontAwesome');
	wp_enqueue_style( 'style-css', get_stylesheet_uri());

	//JS Scripts
	wp_enqueue_script( 'jQuery', '//code.jquery.com/jquery-1.11.3.min.js');
	wp_enqueue_script( 'bootstrap-js-cdn', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js');
	wp_enqueue_script( 'script', '/wp-content/themes/sirenum/js/script.js');
}

add_action( 'wp_enqueue_scripts', 'ofs_scripts' );

add_shortcode('wpv-post-modified', 'wpv_post_modified_shortcode');

function wpv_post_modified_shortcode($atts) {
	if (empty($atts['format'])) {
		$atts['format'] = get_option('date_format');
	}
	return get_the_modified_date($atts['format']);
}

add_shortcode('wpv-post-id', 'wpv_post_id');

function wpv_post_id($atts) {
	if (empty($atts['id'])) {
		$atts['id'] = get_the_ID();
	}
	return get_the_ID();
}

add_shortcode('wpv-category-tree', 'wpv_category_tree_shortcode');

function wpv_category_tree_shortcode() {

	return ofs_theme_get_category_tree();
}

function ofs_theme_get_category_tree() {

	$categories = get_the_category();

	foreach($categories as $category){
		//$cat_link = get_category_link($category->cat_ID);
		$cat_parents = get_category_parents($category->cat_ID, false,'<i class="fa fa-chevron-right"></i>');

		return '<span class="category-label"><i class="fa fa-folder"></i></span>'. $cat_parents;	
	} 
}


function ofs_register_menus() {
  register_nav_menu( 'primary', __( 'Primary Menu', 'ofs' ) );
  register_nav_menu( 'social', __( 'Social Menu', 'ofs' ) );
}
add_action( 'after_setup_theme', 'ofs_register_menus' );

function ofs_register_widgets() {
	register_sidebar( array(
		'name' => 'Homepage Hero',
		'id'   => 'homepage_hero',
		'before_widget' => '<div class="widget home-widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	));
	register_sidebar( array(
		'name' => 'Categories',
		'id'   => 'categories',
		'before_widget' => '<div class="widget home-widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	));
	register_sidebar( array(
		'name' => 'Announcements',
		'id'   => 'announcements',
		'before_widget' => '<div class="widget home-widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	));
	register_sidebar( array(
		'name' => 'Recent',
		'id'   => 'recent',
		'before_widget' => '<div class="widget home-widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	));
	register_sidebar( array(
		'name' => 'Search Sidebar',
		'id'   => 'search_sidebar',
		'before_widget' => '<div class="widget sidebar-widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	));
	register_sidebar( array(
		'name' => 'Footer Left',
		'id'   => 'footer_left',
		'before_widget' => '<div class="widget footer-widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	));
	register_sidebar( array(
		'name' => 'Footer Center',
		'id'   => 'footer_center',
		'before_widget' => '<div class="widget footer-widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	));
	register_sidebar( array(
		'name' => 'Footer Right',
		'id'   => 'footer_right',
		'before_widget' => '<div class="widget footer-widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	));
}
add_action( 'widgets_init', 'ofs_register_widgets' );



function ofs_theme_customizer( $wp_customize ) {
    $wp_customize->add_section( 'ofs_header_section' , array(
    'title'       => __( 'Header', 'ofs' ),
    'priority'    => 30,
    'description' => 'Customize the header Logo and background-color',
	) );


	$wp_customize->add_setting( 'ofs_logo' );
	$wp_customize->add_setting( 'ofs_nav_color' );

	$wp_customize->add_control( 
		new WP_Customize_Image_Control( 
			$wp_customize, 
			'ofs_logo', 
			array(
			    'label'    => __( 'Logo', 'ofs' ),
			    'section'  => 'ofs_header_section',
			    'settings' => 'ofs_logo',
			)
		) 
	);

	$wp_customize->add_control( 
		new WP_Customize_Color_Control( 
			$wp_customize, 
			'ofs_nav_color', 
			array(
			    'label'    => __( 'Header Color', 'ofs' ),
			    'section'  => 'ofs_header_section',
			    'settings' => 'ofs_nav_color',
			) 
		) 
	);
}
add_action( 'customize_register', 'ofs_theme_customizer' );

function ofs_theme_disclaimer_modal($body, $title) {
	$modal = include('modal.php');

	echo $modal;
}


//Hide tags from the admin 
function my_columns_filter( $columns ) {

    unset($columns['tags']);

    return $columns;
}
add_filter( 'manage_edit-video_columns', 'my_columns_filter', 10, 1 );
add_filter( 'manage_edit-files_columns', 'my_columns_filter', 10, 1 );
add_filter( 'manage_edit-image_columns', 'my_columns_filter', 10, 1 );
