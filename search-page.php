<?php 
/**
 * Template Name: Search Page
 *
 */

get_header(); ?>
	<?php if (have_posts()) while (have_posts()) : the_post(); ?>
		<div class="container">
			<div class="row">
				<div class="col-xs-4">
					<?php dynamic_sidebar('search_sidebar'); ?>
				</div>
				<div class="col-xs-8 search">
					<?php the_title('<h1>', '</h1>'); ?>
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	<?php endwhile; ?>

<?php get_footer(); ?>